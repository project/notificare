Notificare is a third party service that provides notifications with and
iPhone and iPad apps. This module enables your Drupal website to use this
service through the Rules module. Simply create a new rule and add the action
"Send message using Notificare Service".

Dependencies

* Rules Entity API (Rules dependency)
* Optional install the Token module, to use tokens in the configured message.

Example use

Create a message when a new comment is created. You'll get a
message in your Notificare App with the option to go to the node or comment.

Installation

Get a Notificare Webhook Service URL

* Create an account at https://app.notifica.re
* Go straight to your Notificare Dashboard and create a Webhook service.
* After choosing a name a selecting a sound, copy the Service URL.

Configuring the notification

* Enable this module and the Rules module (if not installed yet).
* Go to the Notificare configuration page (/admin/config/workflow/notificare).
* Enter the Notificare Service URL.
* Create a new rule, choose 'Send message using Notificare Service' as action.
* Done!

See the official documentation on installing contributed modules at:
http://drupal.org/documentation/install/modules-themes/modules-7
