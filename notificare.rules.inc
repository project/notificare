<?php
/**
 * @file
 * The Rules module implementation
 */


/**
 * Implements hook_rules_action_info().
 */
function notificare_rules_action_info() {

  $actions['notificare_notify'] = array(
    'label' => t('Send message using Notificare Service'),
    'group' => t('Notificare'),
    'access callback' => 'rules_system_integration_access',
    'parameter' => array(
      'subject' => array(
        'type' => 'text',
        'label' => t('Subject'),
        'description' => t("The subject."),
      ),
      'message' => array(
        'type' => 'text',
        'label' => t('Message'),
        'description' => t("The message."),
      ),
      'node' => array(
        'type' => 'node',
        'label' => t('Node'),
      ),
      'comment' => array(
        'type' => 'comment',
        'label' => t('Comment'),
        'optional' => TRUE,
      ),
    ),
  );

  return $actions;
}

/**
 * Gather the info to send to Notificare.
 */
function notificare_notify($subject, $message, $node, $comment = NULL) {

  if (isset($node->nid) || isset($comment->cid)) {
    $type = isset($comment->cid) ? 'comment' : 'node';

    // Create the response URIs.
    switch ($type) {
      case 'comment':
        $payload = notificare_make_payload($type, $subject, $message, $comment);
        break;

      case 'node':
        $payload = notificare_make_payload($type, $subject, $message, $node);
        break;
    }
  }
  else {
    // Simple message with no actions.
    $payload = array(
      "message" => $subject,
      "fullMessage" => $message,
    );
  }
  $result = notificare_send_notificare_message($payload);
}

/**
 * Determine which actions to offer.
 */
function notificare_make_payload($type, $subject, $message, $object) {
  global $base_url;
  $action_url = $base_url . '/notificare/';

  switch ($type) {
    case 'comment':
      $page_url = $base_url . '/comment/' . $object->cid;
      $payload = array(
        "message" => $subject,
        "fullMessage" => $message,
        "targets" => array(
          array(
            "action" => "go to page",
            "url" => $page_url,
            "type" => "url",
            "message" => "false",
          ),
        ),
      );
      break;

    case 'node':
      $page_url = $base_url . '/node/' . $object->nid;
      $payload = array(
        "message" => $subject,
        "fullMessage" => $message,
        "targets" => array(
          array(
            "action" => "go to page",
            "url" => $page_url,
            "type" => "url",
            "message" => "false",
          ),
        ),
      );
      break;
  }

  return $payload;
}

/**
 * Use drupal_http_request to send message to Notificare.
 */
function notificare_send_notificare_message($payload) {

  if (variable_get('notificare_service_url', '') != '') {
    $post_url = variable_get('notificare_service_url', '');

    $options = array(
      'method' => 'POST',
      'timeout' => 15,
      'data' => http_build_query($payload),
      'headers' => array('Content-Type' => 'application/x-www-form-urlencoded'),
    );

    $result = drupal_http_request($post_url, $options);
    if ($result->code != 200) {
      watchdog('notificare', 'Error sending message. Error: %error',
        array(
          '%error' => $result->code,
        ), WATCHDOG_ERROR);
    }
    return $result;
  }
}
